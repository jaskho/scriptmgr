#!/bin/bash


# get command
CMD="$1"
shift


S_SCRIPT_PATH="$(dirname "$(readlink -f "$0")")"
S_PATH=$(grep SCRIPTS_PATH "$S_SCRIPT_PATH/.config" | sed 's/^.*=//')

export S_SCRIPT_PATH S_PATH

case "$CMD" in
  # remove spaces from file/dir names
  despace)
    ;&
  list)
    "$S_SCRIPT_PATH"/list "$@"
    ;;

  samples)
    "$S_SCRIPT_PATH"/generate_sample_previews.sh "$@"
    ;;

  tree)
    "$S_SCRIPT_PATH"/get_dir_tree.sh "$@"
    ;;

  *)
    echo 'Invalid command'
    exit 1
esac

